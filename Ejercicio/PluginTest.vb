﻿Imports Addon2Core
'Imports Addon2Core.DataEngine
'Imports Addon2Core.UI
'Imports Addon2Core.ConfigEngine
'Imports Addon2Core.InitEngine

<Addon2Core.PluginEngine.Plugin("PluginEjercicio", "TipoTest", "DescripciónTest", "Gonzalo García", "0.1", False, "DB1.0")> _
Public Class PluginTest

    Inherits Addon2Core.PluginEngine.CPlugin

    'Clase que se ejecuta en el tiempo de ejecución del plugin
    Public Overrides Sub Run()

        'Conectar con el engine de Addons de SAP
        ControlsSAP.SapUI.AddOn.AddonConnection()

        'Registrar los plugins con Addon2Core, del tipo que sean; en este caso Form y CB1Form
        Addon2Core.UI.Forms.CB1Form.RegisterFormClass(GetType(comprob_NIF))
        Addon2Core.UI.Forms.CB1Form.RegisterFormClass(GetType(comprob_Factura))
        Addon2Core.UI.Forms.CB1Form.RegisterFormClass(GetType(comprob_totalFactura))

    End Sub

End Class
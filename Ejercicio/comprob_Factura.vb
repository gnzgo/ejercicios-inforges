﻿Imports Addon2Core
Imports Addon2Core.DataEngine
Imports Addon2Core.UI
Imports Addon2Core.ConfigEngine
Imports Addon2Core.InitEngine
Imports Addon2Core.UI.Forms
Imports Addon2Core.UI.Controls

'La declaración del tipo de docuemento al que afecta el plugin
<B1FormTypeExAttribute("133")> _
Public Class comprob_Factura

    'Hereda de la clase que define un documento-formulario base (de Addon2Core)
    Inherits CB1Form

    'Declaración de variables para definir un campo de texto y dos botones
    Private WithEvents _totalEurosCampo As CB1Edit
    Private WithEvents _campoCódigo As CB1Edit
    Private WithEvents _campoNombre As CB1Edit
    Private WithEvents _botónAñadir As CB1Button

    'Crear instancia de la clase padre (ésta) + llamar a la inicialización
    Public Sub New(ByVal UID As String)

        MyBase.New(UID)
        Initialize()

    End Sub

    'En la preinicialización es cuando se definen los botones/campos para crearlos en el propio formulario
    Protected Overrides Sub OnPreinitializeComponents()

        MyBase.OnPreinitializeComponents()

        'Aquí van los botones, campos, etc.
        _totalEurosCampo = New CB1Edit(Me, "29")
        _campoCódigo = New CB1Edit(Me, "4")
        _campoNombre = New CB1Edit(Me, "54")
        _botónAñadir = New CB1Button(Me, "1")

    End Sub

    Private Sub Comprobación()

        'Variable del datatable que almacenará el resultado de la query SQL, y la query literal en sí
        Dim clienteFicha As DataTable
        Dim sql As String = "SELECT * FROM OCRD WHERE CardCode = @$0"

        'Try, para manejar cualquier excepción que pudiera suceder en la ejecución
        Try

            'Esto abre la conexión a la BBDD y la cierra al terminar el Using
            Using conn As New Addon2Core.DataEngine.CDBConnection()
                clienteFicha = conn.ExecQuery(sql, _campoCódigo.Value)
            End Using

            'Evaluaciones individuales de las condiciones que exige el ejercicio
            Dim nombreCoincide As Boolean = String.Equals(_campoNombre.Value, clienteFicha.Rows(0).Item("CardName").ToString)
            Dim CIFcoincide As Boolean = Not String.IsNullOrEmpty(clienteFicha.Rows(0).Item("LicTradNum").ToString)
            Dim valorSobre400 As Boolean = (_totalEurosCampo.Value.Substring(0, _totalEurosCampo.Value.Length - 4)) > 399

            'Condicionales con las evaluaciones previas, con su jerarquía particular:
            If Not valorSobre400 Then

                If Not nombreCoincide Then
                    Addon2Core.Addon.CB1App.MessageBox("Error: el nombre no coincide.")
                End If

                If Not CIFcoincide Then
                    Addon2Core.Addon.CB1App.MessageBox("Error: transacción al contado, ya que el cliente no tiene CIF.")
                End If

            Else

                Addon2Core.Addon.CB1App.MessageBox("Error: el valor es más de 400 euros.")

            End If

        Catch ex As Addon2Core.EventEngine.AbortEventException

            Addon2Core.ExceptionEngine.CExceptionManager.Publish(ex)

        End Try

    End Sub

    'Handlers de eventos particulares (validate en campo de precio total; beforeDataAdd para actuar tras pulsar "añadir"
    Private Sub _totalEurosCampo_OnAfterValidate(sender As Object, e As EventEngine.CB1ItemEventArgs) Handles _totalEurosCampo.OnAfterValidate_Inner
        Comprobación()
    End Sub

    Private Sub comprob_Factura_OnBeforeDataAdd(sender As Object, e As EventEngine.CB1FormDataEventArgs) Handles Me.OnBeforeDataAdd
        Comprobación()
    End Sub

End Class

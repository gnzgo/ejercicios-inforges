﻿Imports Addon2Core
Imports Addon2Core.DataEngine
Imports Addon2Core.UI
Imports Addon2Core.ConfigEngine
Imports Addon2Core.InitEngine
Imports Addon2Core.UI.Forms
Imports Addon2Core.UI.Controls

<B1FormTypeExAttribute("134")> _
Public Class comprob_NIF

    'Hereda del documento-formulario base (de Addon2Core)
    Inherits CB1Form

    'Declaración de variables para definir un campo de texto y dos botones
    Private WithEvents _NIF As CB1Edit
    Private WithEvents _botonCancelar As CB1Button
    Private WithEvents _botonRellenar As CB1Button

    'Inicializar la instnacia de la clase dentro la propia clase
    Public Sub New(ByVal UID As String)

        MyBase.New(UID)
        Initialize()

    End Sub

    'Hace el override pero llamando a la clase padre; MyBase es la instancia de la clase padre actual
    Protected Overrides Sub OnInitializeComponents()

        MyBase.OnInitializeComponents()

    End Sub

    'En la preinicialización es cuando se definen los botones/campos para crearlos en el propio formulario
    Protected Overrides Sub OnPreinitializeComponents()

        MyBase.OnPreinitializeComponents()

        'Caja de texto editable
        _NIF = New CB1Edit(Me, "41")

        'Asignación de botones específicos del formulario
        _botonCancelar = New CB1Button(Me, "2")
        _botonRellenar = New CB1Button(Me, _botonCancelar.Control.Left + _botonCancelar.Control.Width + 3, _botonCancelar.Control.Top, _botonCancelar.Control.Width, _botonCancelar.Control.Height)
        _botonRellenar.Caption = "Rellenar"

    End Sub


    Private Sub _botonRellenar_OnAfterClick(sender As Object, e As EventEngine.CB1ItemEventArgs) Handles _botonRellenar.OnAfterClick

        Addon2Core.Addon.CB1App.MessageBox("Mensajito")

    End Sub

    'Método que se ejecuta tras "validar" (al perder el focus el campo actual en teoría; en realidad al pulsar enviar)
    Private Sub _NIF_OnAfterValidate(sender As Object, e As EventEngine.CB1ItemEventArgs) Handles _NIF.OnAfterValidate

        'Try, para manejar cualquier excepción que pudiera suceder en la ejecución
        Try

            'Declarar DataTable; objeto que contiene resultados de una query de base de datos
            Dim datos As DataTable

            'Declarar query como string literal
            Dim sql As String = "SELECT CardCode FROM OCRD WHERE LicTradNum = @$0"

            'Esto abre la conexión a la BBDD y la cierra al terminar el Using
            Using conn As New Addon2Core.DataEngine.CDBConnection()
                datos = conn.ExecQuery(sql, _NIF.Value)
            End Using

            'Si el NIF introducido no es es el especial Y ya existe un NIF como el introducido, aconsejar
            If _NIF.Value <> "ES1234567X" And datos.Rows.Count <> 0 Then
                Addon2Core.Addon.CB1App.MessageBox("Ya existe el DNI; prueba con el ES1234567X que funciona siempre.")
            End If

        Catch ex As Addon2Core.EventEngine.AbortEventException

            Addon2Core.ExceptionEngine.CExceptionManager.Publish(ex)

        End Try

    End Sub

End Class

﻿Imports Addon2Core
Imports Addon2Core.DataEngine
Imports Addon2Core.UI
Imports Addon2Core.ConfigEngine
Imports Addon2Core.InitEngine
Imports Addon2Core.UI.Forms
Imports Addon2Core.UI.Controls

'La declaración del tipo de docuemento al que afecta el plugin
<B1FormTypeExAttribute("141")> _
Public Class comprob_totalFactura

    'Hereda de la clase que define un documento-formulario base (de Addon2Core)
    Inherits CB1Form

    'Declaración de variables para definir un campo de texto
    Private WithEvents _cajaDocNum As CB1Edit

    'Crear instancia de la clase padre (ésta) + llamar a la inicialización
    Public Sub New(ByVal UID As String)

        MyBase.New(UID)
        Initialize()

    End Sub

    'En la preinicialización es cuando se definen los botones/campos para crearlos en el propio formulario
    Protected Overrides Sub OnPreinitializeComponents()

        MyBase.OnPreinitializeComponents()

        'Caja de texto que contiene el número de la factura actual
        _cajaDocNum = New CB1Edit(Me, "8")

    End Sub

    'El método que hace lo que pide el ejercicio: sumar los números totales de artículos y sus precios.
    Private Sub SumaDeTotales()

        'Datatables para alojar los resultados de las queries; una para la tabla de la Factura, otra para la del contenido (los artículos)
        Dim docNum As DataTable
        Dim artículos As DataTable

        'Las queries mencionadas antes.
        Dim queryDocEntry As String = "SELECT DocEntry FROM " & Me.DetailTableName & " WHERE DocNum = @$0"
        Dim queryArtículo As String = "SELECT * FROM " & Me.DetailTableName & " WHERE DocEntry = @$0"

        'Try, para manejar cualquier excepción que pudiera suceder en la ejecución
        Try

            'La conexión a la BBDD; se ejecuta la primera query, y usando su resultado, ejecuto entonces la segunda query
            Using conn As New Addon2Core.DataEngine.CDBConnection() 'con IsolationLevel.ReadUncommited no bloquea el registro transaccional
                docNum = conn.ExecQuery(queryDocEntry, _cajaDocNum.Value)

                Dim docEntryActual As String = docNum.Rows(0).Item("DocEntry").ToString

                artículos = conn.ExecQuery(queryArtículo, docEntryActual)
            End Using

            'Un cuenta simple del total de filas (artículos totales) obtenidos como resultado
            Dim totalEntradas As Integer = artículos.Rows.Count

            'Declaración de variables de total de precios y total de artículos
            Dim totalArtículos As Double
            Dim totalPrecio As Double

            'Loop FOR sencillo para avanzar de fila en fila, sumando sus Artículos Totales y sus Precios
            For i As Integer = 0 To totalEntradas - 1
                totalArtículos += artículos.Rows(i).Item("Quantity")
                totalPrecio += artículos.Rows(i).Item("Price")
            Next

            'El mensajito final con los resultados.
            Addon2Core.Addon.CB1App.MessageBox("El total de artículos es " & totalArtículos & " y sus precios suman " & totalPrecio & " eurosss.")

        Catch ex As Addon2Core.EventEngine.AbortEventException

            Addon2Core.ExceptionEngine.CExceptionManager.Publish(ex)

        End Try

    End Sub

    'Handlers de evento DataLoad (dispara tras cargar una Factura nueva)
    Private Sub comprob_totalFactura_OnAfterDataLoad(sender As Object, e As EventEngine.CB1FormDataEventArgs) Handles Me.OnAfterDataLoad
        SumaDeTotales()
    End Sub

End Class

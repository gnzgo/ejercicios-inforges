﻿Imports Addon2Core
Imports Addon2Core.DataEngine.B1
Imports Addon2Core.InitEngine
Imports Addon2Core.UI

<Addon2Core.PluginEngine.Plugin("ejercicioAsistente", "IFG", "Plugin de X", "Gonzalo García", "0.1", False, "DB1.0")>
Public Class generarAsistente

    'Herencia de la clase CPlugin e implementación de la interfaz ICreacionCampoSap
    Inherits Addon2Core.PluginEngine.CPlugin
    Implements Addon2Core.InitEngine.InitJobs.ICreacionCamposSap

    'Declaración e instanciación del objeto de menú (en Inventario en este caso, "3072")
    Public Overrides Sub Run()

        ControlsSAP.SapUI.AddOn.AddonConnection()

        Dim miAsistente As New Addon2Core.UI.CB1Menu("EJERCICIOWIZ", "3072", "Asistente del ejercicio", AddressOf asistenteEjercicio_OnMenuClick)

    End Sub

    'Disparador del evento de click en el menú
    <Addon2Core.EventEngine.SynchronizedEvent()>
    Public Sub asistenteEjercicio_OnMenuClick(ByVal sender As Object, ByVal event_info As Addon2Core.EventEngine.CB1MenuEventArgs)

        Dim ventanaAsistente As ejercicioAsistente
        ventanaAsistente = New ejercicioAsistente()

        Addon2Core.Addon.CB1App.EmbedIntoSAP(ventanaAsistente)
        ventanaAsistente.Show()

    End Sub

    'Ejecución de los métodos de creación de tablas (o de lo que se quisiera ejecutar)
    Public Sub Start() Implements Addon2Core.InitEngine.InitJobs.ICreacionCamposSap.Start

        'Me.CrearTablaPersonalizada_NOMBREDETABLA()
        'Me.CrearCamposPersonalizados_SUBTABLA()

    End Sub

    'Creación de tabla principal (cabecera de documento)
    Private Sub CrearTablaPersonalizada_NOMBREDETABLA()

        Dim tablasXML As New CB1Tables
        Dim tabla As CB1Table

        Try
            'Tabla con los datos de cabecera y sus campos
            tabla = tablasXML.AddUserTable("IFG_TablaEjercicio", "Nombre descriptivo", SAPbobsCOM.BoUTBTableType.bott_NoObject)

            tabla.AddAlphaField("IFG_CodigoArt", "Código", Addon2Core.DataEngine.B1.CB1Field.CODE_SIZE)
            tabla.AddAlphaField("IFG_DescriArt", "Descripción", Addon2Core.DataEngine.B1.CB1Field.NAME_SIZE)

            tablasXML.Run()

        Catch ex As Exception

            Throw New Exception("Error en la creación de la tabla: " & ex.Message)

        End Try

    End Sub

    'Creación de subtabla de la principal (elementos de la matriz del documento)
    Private Sub CrearCamposPersonalizados_SUBTABLA()

        Dim tablasXML As New CB1Tables
        Dim tabla As CB1Table

        Try
            'Tabla de BBDD que almacenará los elementos de la matriz del documento y sus campos
            tabla = tablasXML.AddUserTable("IFG_SubTabla", "Nombre descriptivo", SAPbobsCOM.BoUTBTableType.bott_NoObject)

            tabla.AddDateField("IFG_Fecha", "Campo de fecha", DateFieldSubTypes.tDate)
            tabla.AddNumericField("IFG_GrupoArt", "Grupo de artículo")
            tabla.AddFloatField("IFG_Descuento", "Descuento", FloatFieldSubTypes.Percentage)

            'Foreign key que la vinculará a la tabla principal
            tabla.AddAlphaField("CODCABECERA_FK", "Código de la cabecera", 30)

            tablasXML.Run()

        Catch ex As Exception

            Throw New Exception("Error en la creación de la tabla: " & ex.Message)

        End Try

    End Sub

End Class
